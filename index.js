const config = require('./config'),
    webdav = require('webdav-server').v2,
    serveIndex = require('serve-index'),
    express = require('express'),
    app = express(),
    userManager = new webdav.SimpleUserManager(),
    user = userManager.addUser(config.username, config.password, false),
    privilegeManager = new webdav.SimplePathPrivilegeManager(),
    server = new webdav.WebDAVServer({
        requireAuthentification: true,
        httpAuthentication: new webdav.HTTPBasicAuthentication(userManager, 'DAV Auth'),
        privilegeManager: privilegeManager,
        storageManager: new webdav.PerUserStorageManager(config.limit),
        rootFileSystem: new webdav.PhysicalFileSystem(__dirname + '/public')
    })

privilegeManager.setRights(user, '/', ['all'])

app.disable('x-powered-by')
    .use(webdav.extensions.express('/dav', server), express.static(__dirname + '/public'), serveIndex('public/', { 'icons': true, view: 'details' }))
    .listen(config.port, () => {
        console.log('Listening on: ::' + config.port)
    })