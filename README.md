# WebDAV personal server

## Install

- git clone https://gitea.hatthieves.es/manalejandro/webdav
- cd webdav
- npm i

## Run

- change settings in `config.js`
- npm start
- open http://localhost:8080/dav with [cadaver](http://www.webdav.org/cadaver/) and enjoy :)

### License

- MIT